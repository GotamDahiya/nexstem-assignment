# EEG Dataset for Eyeblinks and Jaw Clenches

### Task

Create a Ml/DL model to detect artefacts like Eye Blink and Jaw Clench in the EEG signals. Your model should be able to predict whether the signal corresponds to a Jaw Clench, Eye Blink or nothing. For evaluation, the input will be an EEG recording, and your model should predict the time intervals(using timestamps in the dataset) with Eye Blinks and Jaw Clenches. The submission should consist of your python file, a small report on the performance metrics of your model and also a README on your network architecture as well as usage of the script.

#### Evaluation Criteria
* Innovativeness of the Approach
* Time taken to give the output for the EEG dataset
* Resource Utilisation during Inference
* Network Architecure and Models used

### Dataset Description

The dataset consists of 14-channel EEG data sampled at 128Hz i.e 128 samples per second for 6 minutes. Each data sample contains 19 columns. The dataset consists of 84 markers (2 baseline markers and 82 markers for eye blinks and jaw clenches).

The first 30 seconds of the data consists of baseline data, with the first seconds recorded with eyes open and the remaining 15 with eyes open.
Marker type value helps you reecognise whether the marker is of type instance or interval.

#### Column Description

Column No. | Column Name | Description
---------|----------|---------
 1 | Timestamp | Timestamp when the data was recieved, correct upto deciseconds
 2 | EEG.Counter | Samnple no. (0-128)
 3 | EEG.AF3 | Datapoint for AF3 channel
 4 | EEG.F7 | Datapoint for F7 channel
 5 | EEG.F3 | Datapoint for F3 channel
 6 | EEG.FC5 | Datapoint for FC5 channel
 7 | EEG.T7 | Datapoint for T7 channel
 8 | EEG.P7 | Datapoint for P7 channel
 9 | EEG.O1 | Datapoint for O1 channel
 10 | EEG.O2 | Datapoint for O2 channel
 11 | EEG.P8 | Datapoint for P8 channel
 12 | EEG.T8 | Datapoint for T8 channel
 13 | EEG.FC6 | Datapoint for FC6 channel
 14 | EEG.F4 | Datapoint for F4 channel
 15 | EEG.F8 | Datapoint for F8 channel
 16 | EEG.AF4 | Datapoint for AF4 channel
 17 | MarkerIndex | Marker Index
 18 | MarkerType | Marker Type (1 - Instance, 2 - Interval)
 19 | MarkerValueInt | Value for the Marker(refer to Marker Legend Section)


#### Marker Legend

* 1 -> Eyes Open
* 3 -> Eyes Closed
* 22 -> Eye Blink
* 23 -> Jaw Clench

#### Notes
* Assume a windows size of 0.5 seconds. (0.2 seconds before the marker and 0.3 seconds after the marker)
* The last three columns for data with no markers are NaN.
* You can also refer to the file [S02_21.08.20_14.34.32cortex.marker.json](S02_21.08.20_14.34.32cortex.marker.json) for more details about the markers.
* It is not necessary to use all the channels for detecting the artefacts, you may be better off o by just using the AF3 and AF4 channels.
* For clear understanding of the EEG channel maps refer to [10-20 System](https://en.wikipedia.org/wiki/10%E2%80%9320_system_(EEG)).